package com.example.ezrestpvt;

public interface WaitListener {

    public void checkCondition();

    public void onWaitEnd();

    public void onConditionSuccess();

}