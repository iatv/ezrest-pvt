package com.example.ezrestpvt;

import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends WearableActivity {

    private TextView mTextView;
    private View mainView;
    private View blankView;
    private View stimuliView;
    private View finishView;
    private TextView waitText1,waitText2,waitText3,waitText4,waitText5;
    public int timeMin;
    public int timeMax;
    public int testTime;
    public int testCount=0;
    public int countdown=5;
    public boolean logoTimeEnd;
    public long stimuliStartTime,stimuliEndTime,userStimuliTime;
    public String resultsPVT[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logoTimeEnd=false;
        mTextView = (TextView) findViewById(R.id.text);
        blankView = findViewById(R.id.ezrestpvtblack);
        stimuliView = findViewById(R.id.ezrestpvtstimuli);
        finishView = findViewById(R.id.ezrestpvtfinish);
        mainView = findViewById(R.id.ezrestpvtmain);
        waitText1 = findViewById(R.id.text1);
        waitText2 = findViewById(R.id.text2);
        waitText3 = findViewById(R.id.text3);
        waitText4 = findViewById(R.id.text4);
        waitText5 = findViewById(R.id.text5);
        blankView.setVisibility(View.GONE);
        stimuliView.setVisibility(View.GONE);
        finishView.setVisibility(View.GONE);
        mainView.setVisibility(View.VISIBLE);

        waitLogo();

        // Enables Always-on
        setAmbientEnabled();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        resultsPVT = new String[10];
//        Toast.makeText(getApplicationContext(),"Now onStart() calls", Toast.LENGTH_LONG).show(); //onStart Called

    }
    private int getRandomTime(int min,int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }
    public void waitTimer() {
        final Waiter waiter = new Waiter(getMainLooper(), 1000, 5000);

        waiter.setWaitListener(new WaitListener() {

            @Override
            public void checkCondition() {

                mainView.setVisibility(View.GONE);
                finishView.setVisibility(View.GONE);
                blankView.setVisibility(View.VISIBLE);
                waitText1.setVisibility(View.VISIBLE);
                String numberAsString = String.valueOf(countdown);
                waitText1.setText(numberAsString);
                countdown=countdown-1;
                if (countdown==-2) {
                    waiter.setConditionState(true);
                }

            }

            @Override
            public void onWaitEnd() {

 //               finishView.setVisibility(View.VISIBLE);
                //DO
            }

            @Override
            public void onConditionSuccess() {
;
                waitText1.setVisibility(View.GONE);
                PVTTest();
                //DO
            }

        });

        waiter.start();
    }

    public void PVTTest() {
        if (testCount!=10) {
            int number = getRandomTime(100, 8000);
            Log.i("PVT TEST", "Random Number =" + number);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    stimuliView.setVisibility(View.VISIBLE);
                    stimuliView.requestFocus();
                    /* Using Nano Time instead of SystemTime */
                    stimuliStartTime = System.nanoTime();
                    // Actions to do after number milliseconds
                }
            }, number);
        }else
        {
            finishView.setVisibility(View.VISIBLE);
        }

    }
    public void finishTest (View view) {
        finishAffinity();

        System.exit(0);
    }
    public void PVTClick (View view) {
        /* Using Nano Time instead of SystemTime */
        stimuliEndTime=System.nanoTime();
        userStimuliTime= stimuliEndTime-stimuliStartTime;
        Toast.makeText(getApplicationContext(),"clicked after "+(userStimuliTime/1000000)+" ms "+testCount, Toast.LENGTH_LONG).show(); //onStart Called
        testCount++;
        stimuliView.setVisibility(View.GONE);
        blankView.setVisibility(View.VISIBLE);
    PVTTest();
    }
    public void waitLogo() {
        final Waiter waiter = new Waiter(getMainLooper(), 1000, 2000);

        waiter.setWaitListener(new WaitListener() {

            @Override
            public void checkCondition() {
                Log.i("Connection", "Checking connection...");

            }

            @Override
            public void onWaitEnd() {
                Log.i("Connection", "No connection for sending");
//                finishView.setVisibility(View.VISIBLE);
                logoTimeEnd = true;
                waitTimer();
                //DO
            }

            @Override
            public void onConditionSuccess() {
                Log.i("Connection", "Connection success, sending...");
                //DO
            }

        });

        waiter.start();
    }
}
